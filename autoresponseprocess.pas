unit AutoResponseProcess;

{$mode objfpc}{$H+}

interface

uses
  Classes,SysUtils;

type
  ENilOutput = class(Exception) end;
  EUnequalPromptResponseLength = class(Exception) end;

  TOnIdle = procedure;

procedure ExecProcess(
  const Executable: String;
  const Arguments: array of String;
  Output: TStrings;
  const Prompts: array of String;
  const Responses: array of String;
  const OnIdle: TOnIdle
);

implementation

uses
  Process;

resourcestring
  SOutputMayNotBeNil = 'Output may not be nil';
  SPromptsResponsesMustHaveSameLength = 'Prompts and Responses must have the same length';

procedure ExecProcess(
  const Executable: String;
  const Arguments: array of String;
  Output: TStrings;
  const Prompts: array of String;
  const Responses: array of String;
  const OnIdle: TOnIdle
);

var
  Proc: TProcess;
  Arg,SearchPromptBuf: String;
  BytesAvailCount: LongWord;

  procedure ReadOutputAndHandlePrompt; inline;
  var
    RealLen: LongWord;
    i: Integer;
    TempBuf,Response: String;
  begin
    // read partial output
    SetLength(TempBuf,BytesAvailCount);
    RealLen := Proc.Output.Read(TempBuf[1],BytesAvailCount);
    SetLength(TempBuf,RealLen);
    // flush output
    Output.Text := Output.Text + TempBuf;
    // append to search buffer
    SearchPromptBuf := SearchPromptBuf + TempBuf;
    // check for prompt and answer if found
    for i := Low(Prompts) to High(Prompts) do
      if Pos(Prompts[i],SearchPromptBuf) > 0 then begin
        Response := Responses[i] + LineEnding;
        Proc.Input.Write(Response[1],Length(Response));
        { clean search prompt buffer so the same prompt won't be detected
          again (unless it appears again later from next output) }
        SearchPromptBuf := '';
        // assume only one prompt can match at a time
        Break;
      end;
  end;

begin
  if not Assigned(Output) then
    raise ENilOutput.Create(SOutputMayNotBeNil);
  if Length(Prompts) <> Length(Responses) then
    raise EUnequalPromptResponseLength.Create(SPromptsResponsesMustHaveSameLength);
  // prepare process options and arguments
  Proc := TProcess.Create(nil);
  Proc.Options := [poUsePipes,poNoConsole,poStderrToOutPut];
  Proc.ShowWindow := swoHIDE;
  Proc.Executable := Executable;
  for Arg in Arguments do
    Proc.Parameters.Add(Arg);
  // begin process
  try
    Output.Text := '';
    SearchPromptBuf := '';
    Proc.Execute;
    while Proc.Running do begin
      BytesAvailCount := Proc.Output.NumBytesAvailable;
      if BytesAvailCount > 0 then
        ReadOutputAndHandlePrompt
      else if Assigned(OnIdle) then
        OnIdle;
    end;
    // process dies, read the rest of output
    repeat
      BytesAvailCount := Proc.Output.NumBytesAvailable;
      if BytesAvailCount > 0 then
        // actually, no prompt is possible here, but it doesn't hurt
        ReadOutputAndHandlePrompt
    until BytesAvailCount <= 0;
  finally
    Proc.Free
  end;
end;

end.

