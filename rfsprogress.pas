unit rfsprogress;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, Messages;

type

  { TfrmProgress }

  TfrmProgress = class(TForm)
    lblLineCount: TLabel;
    memMain: TMemo;
    lblInfo: TLabel;
    pbMain: TProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure memMainChange(Sender: TObject);
  private
    fLineCount: integer;
    fOldLineCount: integer;
  public
    //property Memo: TMemo read memMain write memMain;
    //property Info: TLabel read lblInfo write lblInfo;
    //property ProgressBar: TProgressBar read pbMain write pbMain;
  end;

var
  frmProgress: TfrmProgress;

implementation

{$R *.lfm}

{ TfrmProgress }

procedure TfrmProgress.memMainChange(Sender: TObject);
begin
  //memMain.SelStart := memMain.GetTextLen;
  //memMain.SelLength := 0;
  //memMain.Perform(EM_LINESCROLL, 0, 0);

  //Handle OnChange in TMemo, set SelStart to Length(Memo1.Lines.Length) - 1
  memMain.SelStart := Length(memMain.Text) - 1;

  lblLineCount.Caption := 'Lines = ' + IntToStr(memMain.Lines.Count);
  pbMain.Position := memMain.Lines.Count;
  //fLineCount := fLineCount + 1;
  //pbMain.Position := ;
  //if fOldLineCount <> memMain.Lines.Count then
  //  begin
  //    fOldLineCount := memMain.Lines.Count;
  //    pbMain.Position := pbMain.Position + 1;
  //  end;
end;

procedure TfrmProgress.FormCreate(Sender: TObject);
begin
  fLineCount := 0;
end;

end.

