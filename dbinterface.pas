unit dbinterface;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, RockFSCB, RockFSTypes;

type

  { TDBInterface }

  TDBInterface = class
  private
    fRockFSCB: TRockFSCB;
  public
    constructor Create;
    destructor Destroy; override;

    // File transfer and server manipulation
    procedure RemoveCDFromAPTSources(const IPAddress,Password: string);
    procedure ChangeIPAddress(const IPAddress,Password,NewIPAddress,Netmask,Gateway: string);
    function  FreeSWITCHSourcesInAPTSources(const IPAddress,Password: string): boolean;
    procedure AddFreeSWITCHSourcesToAPTSourcesAndInstall(const IPAddress,Password: string);
    procedure GetConfFilesFromServer(const IPAddress,Password,ConfSrc,ConfDest: string);
    procedure GetFileFromServer(const IPAddress,Password,SrcFolder,SrcFile,DestFolder: string);
    procedure PutFileOnServer(const IPAddress,Password,SrcFolder,SrcFile,DestFolder: string);
    procedure PutFileOnServerEx(const IPAddress,Password,SrcFolder,SrcFile,DestFolder: string);
    procedure IssueCommandToServer(const IPAddress,Password,Command: string);
    procedure IssueCommandToServerNoWait(const IPAddress,Password,Command: string);
    procedure ReloadXML(const IPAddress,Password: string);
  end;

implementation

uses
  StrUtils, process;


constructor TDBInterface.Create;
begin
  inherited Create;
  fRockFSCB := TRockFSCB.Create;
  fRockFSCB.Read;
end;

//********************************************************************************************************************
// Destroy
//********************************************************************************************************************

destructor TDBInterface.Destroy;
begin
  fRockFSCB.Free;
  inherited Destroy;
end;

procedure TDBInterface.RemoveCDFromAPTSources(const IPAddress,Password: string);
var
  I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile,sSrcFolder,sSrcFile,sDestFolder: string;
  Process: TProcess;
begin
  // This code will retrieve the /etc/apt/source.list file from the server.
  // Then, it will comment out the "deb cdrom" line (if needed)
  // Then, upload it again to the server

  sSrcFolder := '/etc/apt/';
  sSrcFile := 'sources.list';
  sDestFolder := IncludeTrailingBackslash(fRockFSCB.WorkingDir);

  GetFileFromServer(IPAddress,Password,sSrcFolder,sSrcFile,sDestFolder);

  if not FileExists(sDestFolder + sSrcFile) then // Check to make sure we got the file
    raise Exception.Create('Unable to find the sources.list file');

  // OK, we now have the sources file, so comment out the CD 1 if need be.
  SL := TStringList.Create;
  try
    SL.LoadFromFile(sDestFolder + sSrcFile);
    for I := 0 to SL.Count -1 do
      begin
        S := SL[I];
        if AnsiContainsStr(S,'deb cdrom:') then
          if not AnsiStartsStr('#',S) then
            begin
              // We've found an entry that hasn't been commented out
              S := '#' + S;
              SL[I] := S;
              SL.SaveToFile(sDestFolder + sSrcFile);
            end;
      end;
  finally
    SL.Free;
  end;
  PutFileOnServer(IPAddress,Password,sDestFolder,sSrcFile,sSrcFolder);
end;

procedure TDBInterface.ChangeIPAddress(const IPAddress,Password,NewIPAddress,
  Netmask,Gateway: string);
var
  I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile,sSrcFolder,sSrcFile,sDestFolder: string;
  //Process: TProcess;
  bDynamicIPFound,bStaticIPFound: boolean;
begin
  // First, get the /etc/network/interfaces file
  // Then, it will comment out the "iface eth0 inet dhcp" line (if needed)
  // Then, it will insert the following lines in this order
  //     gateway 192.168.0.254
  //     netmask 255.255.255.0
  //     address 192.168.0.7
  // iface eth0 inet static
  // Then, upload it again to the server

  bDynamicIPFound := False;
  bStaticIPFound := False;
  sSrcFolder := '/etc/network/';
  sSrcFile := 'interfaces';
  sDestFolder := IncludeTrailingBackslash(fRockFSCB.WorkingDir);

  GetFileFromServer(IPAddress,Password,sSrcFolder,sSrcFile,sDestFolder);

  if not FileExists(sDestFolder + sSrcFile) then // Check to make sure we got the file
    raise Exception.Create('Unable to find the sources.list file');

  // OK, we now have the interfaces file, so comment out the "iface eth0 inet dhcp" if needed.
  SL := TStringList.Create;
  try
    SL.LoadFromFile(sDestFolder + sSrcFile);
    for I := 0 to SL.Count -1 do
      begin
        S := SL[I];
        if (AnsiContainsStr(S,'iface eth0 inet dhcp')) and (Copy(S,1,1)<>'#') then
          begin
            bDynamicIPFound := True;
            // We've found an entry that hasn't been commented out
            S := '#' + S;
            SL[I] := S;
            // Now add the static lines
            S := '     gateway ' + Gateway;
            SL.Insert(I + 1,S);
            S := '     netmask ' + Netmask;
            SL.Insert(I + 1,S);
            S := '     address ' + NewIPAddress;
            SL.Insert(I + 1,S);
            S := 'iface eth0 inet static';
            SL.Insert(I + 1,S);
            SL.SaveToFile(sDestFolder + sSrcFile);
          end
        else
          begin
          // Look for uncommented static definition
            if (AnsiContainsStr(S,'iface eth0 inet static')) and (Copy(S,1,1)<>'#') then
              begin
                bStaticIPFound := True;
                break;
              end;
          end;
      end;

    if bStaticIPFound then
      begin
        for I := 0 to SL.Count -1 do
          begin
            S := SL[I];
            if AnsiContainsStr(S,'address') then
              begin
                S := '     address ' + NewIPAddress;
                SL[I] := S;
              end;
            if AnsiContainsStr(S,'netmask') then
              begin
                S := '     netmask ' + Netmask;
                SL[I] := S;
              end;
            if AnsiContainsStr(S,'gateway') then
              begin
                S := '     gateway ' + Gateway;
                SL[I] := S;
              end;
          end;
        SL.SaveToFile(sDestFolder + sSrcFile);
      end
    else
      begin
        raise Exception.Create('Unable to find the line "iface eth0 inet static"');
      end;
  finally
    SL.Free;
  end;
  if not bDynamicIPFound then   // This is not good as we don't know what there network card is called.
    raise Exception.Create('Unable to find the line "iface eth0 inet dhcp"');

  PutFileOnServer(IPAddress,Password,sDestFolder,sSrcFile,sSrcFolder);
end;

function TDBInterface.FreeSWITCHSourcesInAPTSources(const IPAddress,
  Password: string): boolean;
var
  bFound: boolean;
  I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile,sSrcFolder,sSrcFile,sDestFolder: string;
  //Process: TProcess;
begin
  bFound := False;

  // This code will retrieve the /etc/apt/source.list file from the server.
  // Then, it will check for "freeswitch sources" line
  // Then, return true if found.

  sSrcFolder := '/etc/apt/sources.list.d/';
  sSrcFile   := 'freeswitch.list';
  sDestFolder := IncludeTrailingBackslash(fRockFSCB.WorkingDir);

  GetFileFromServer(IPAddress,Password,sSrcFolder,sSrcFile,sDestFolder);

  if not FileExists(sDestFolder + sSrcFile) then // Check to make sure we got the file
    begin
      bFound := False;
      Result := bFound;
      Exit;
    end;

  // OK, we now have the sources file, so check for freeswitch sources.
  SL := TStringList.Create;
  try
    SL.LoadFromFile(sDestFolder + sSrcFile);
    for I := 0 to SL.Count -1 do
      begin
        S := SL[I];
        if AnsiContainsStr(S,'deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ '+ cDebianDistro + ' main') then
          begin
            bFound := True;
            Break;
          end;
      end;
  finally
    SL.Free;
  end;
  Result := bFound;
end;

procedure TDBInterface.AddFreeSWITCHSourcesToAPTSourcesAndInstall(
  const IPAddress,Password: string);
var
  sSrcFile,sSrcFolder,sDstFolder: string;
begin
  sSrcFolder := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
  sSrcFile   := cFSInstallScriptFile;
  sDstFolder := '/var/tmp/';

  // First, make sure we have our script that will actually do the work...
  if not FileExists(sSrcFolder + sSrcFile) then
    raise Exception.Create('Unable to find the file "' + sSrcFolder + sSrcFile + '"');

  PutFileOnServer(IPAddress,Password,sSrcFolder,sSrcFile,sDstFolder);
  IssueCommandToServer(IPAddress,Password,'chmod +x ' + sDstFolder + cFSInstallScriptFile);
  IssueCommandToServer(IPAddress,Password,sDstFolder + cFSInstallScriptFile);

  //fDBInterface.IssueCommandToServer(sUser,sIPAddress,sPassword,'wget http://www.rocksolidvoip.co.uk/downloads/fs-debian-installer-pkgs.sh');
  //fDBInterface.IssueCommandToServer(sUser,sIPAddress,sPassword,'chmod +x ./fs-debian-installer-pkgs.sh && ./fs-debian-installer-pkgs.sh');
  //fRemoteWorkComplete := True;
  //fDBInterface.IssueCommandToServer(sUser,sIPAddress,sPassword,'reboot');
end;

procedure TDBInterface.GetConfFilesFromServer(const IPAddress,Password,ConfSrc,
  ConfDest: string);
var
  SL: TStringList;
  S,sBatchFile,sPSFTPFile: string;
  Process: TProcess;
begin
  // We need to create a batch file, and then enter a command to run it.
  // psftp user@serverip -pw Password -b batchfile.scr
  //
  // Batch file will be

  // get /usr/local/freeswitch/conf c:\downloads\
  // quit

  // First, let's create the batchfile
  SL := TStringList.Create;
  try
    S := 'get -r ' + '"' + ConfSrc + '" "' + ConfDest + '"';
    SL.Add(S);
    SL.Add('quit');
    sBatchFile := IncludeTrailingBackslash(fRockFSCB.WorkingDir) + cPSFTPBatchFile;
    SL.SaveToFile(sBatchFile);
  finally
    SL.Free;
  end;
  Process := TProcess.Create(nil);
  try
    sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'psftp.exe';
    Process.Executable := sPSFTPFile;
    S := cRootUser + '@' + IPAddress + ' -pw ' + Password + ' -b ' + '"' + sBatchFile + '"';
    Process.Parameters.Add(S);
    //Process.Options := Process.Options + [poWaitOnExit, poUsePipes];
    Process.Options := Process.Options + [poWaitOnExit];
    Process.Execute;

    {SLOutput := TStringList.Create;
    try
      SLOutput.LoadFromStream(Process.Output);
      // Save the output to a file.
      SLOutput.SaveToFile(IncludeTrailingBackslash(fRockFSCB.WorkingDir) + 'output.txt');
    finally
      SLOutput.Free;
    end;}
  finally
    Process.Free;
  end;
end;

procedure TDBInterface.GetFileFromServer(const IPAddress,Password,SrcFolder,
  SrcFile,DestFolder: string);
var
  //I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile: string;
  Process: TProcess;
begin
  // First, make sure that the psftp.exe file exists in the current directory
  sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'psftp.exe';
  if not FileExists(sPSFTPFile) then
    raise Exception.Create('Unable to find the file "' + sPSFTPFile + '"');

  // This code will get a file from the server.

  // First, let's create the batchfile
  SL := TStringList.Create;
  try
    S := 'cd ' + '"' + SrcFolder + '"';
    SL.Add(S);
    S := 'lcd ' + '"' + DestFolder + '"';
    SL.Add(S);
    S := 'get ' + '"' + SrcFile + '"'; // "' + sDestFolder + '"';
    SL.Add(S);
    SL.Add('quit');
    sBatchFile := IncludeTrailingBackslash(fRockFSCB.WorkingDir) + cPSFTPBatchFile;
    SL.SaveToFile(sBatchFile);
  finally
    SL.Free;
  end;
  Process := TProcess.Create(nil);
  try
    Process.Executable := sPSFTPFile;
    S := cRootUser + '@' + IPAddress + ' -pw ' + Password + ' -b ' + '"' + sBatchFile + '"';
    Process.Parameters.Add(S);
    //Process.Options := Process.Options + [poWaitOnExit, poUsePipes];
    Process.Options := Process.Options + [poWaitOnExit];
    Process.Execute;

    {SLOutput := TStringList.Create;
    try
      SLOutput.LoadFromStream(Process.Output);
      // Save the output to a file.
      SLOutput.SaveToFile(IncludeTrailingBackslash(fRockFSCB.WorkingDir) + 'output.txt');
    finally
      SLOutput.Free;
    end;}
  finally
    Process.Free;
  end;
end;

procedure TDBInterface.PutFileOnServer(const IPAddress,Password,SrcFolder,
  SrcFile,DestFolder: string);
var
  //I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile: string;
  Process: TProcess;
begin
  // This code will put a file on the server.

  // First, let's create the batchfile
  SL := TStringList.Create;
  try
    S := 'lcd ' + '"' + SrcFolder + '"';
    SL.Add(S);
    S := 'cd ' + '"' + DestFolder + '"';
    SL.Add(S);
    S := 'put ' + '"' + SrcFile + '"'; // "' + sDestFolder + '"';
    SL.Add(S);
    SL.Add('quit');
    sBatchFile := IncludeTrailingBackslash(fRockFSCB.WorkingDir) + cPSFTPBatchFile;
    SL.SaveToFile(sBatchFile);
  finally
    SL.Free;
  end;
  Process := TProcess.Create(nil);
  try
    sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'psftp.exe';
    Process.Executable := sPSFTPFile;
    S := cRootUser + '@' + IPAddress + ' -pw ' + Password + ' -b ' + '"' + sBatchFile + '"';
    Process.Parameters.Add(S);
    //Process.Options := Process.Options + [poWaitOnExit, poUsePipes];
    Process.Options := Process.Options + [poWaitOnExit];
    Process.Execute;

    {SLOutput := TStringList.Create;
    try
      SLOutput.LoadFromStream(Process.Output);
      // Save the output to a file.
      SLOutput.SaveToFile(IncludeTrailingBackslash(fRockFSCB.WorkingDir) + 'output.txt');
    finally
      SLOutput.Free;
    end;}
  finally
    Process.Free;
  end;
end;

procedure TDBInterface.PutFileOnServerEx(const IPAddress,Password,SrcFolder,
  SrcFile,DestFolder: string);
var
  //I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile: string;
  Process: TProcess;
  OurCommand: String;
  OutputLines: TStringList;
  MemStream: TMemoryStream;
  OurProcess: TProcess;
  NumBytes: LongInt;
  BytesRead: LongInt;
const
  READ_BYTES = 2048;

begin
  // This code will put a file on the server.

  // First, let's create the batchfile
  SL := TStringList.Create;
  try
    S := 'lcd ' + '"' + SrcFolder + '"';
    SL.Add(S);
    S := 'cd ' + '"' + DestFolder + '"';
    SL.Add(S);
    S := 'put ' + '"' + SrcFile + '"'; // "' + sDestFolder + '"';
    SL.Add(S);
    SL.Add('quit');
    sBatchFile := IncludeTrailingBackslash(fRockFSCB.WorkingDir) + cPSFTPBatchFile;
    SL.SaveToFile(sBatchFile);
  finally
    SL.Free;
  end;
  Process := TProcess.Create(nil);
  try
    sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'psftp.exe';
    Process.Executable := sPSFTPFile;
    S := cRootUser + '@' + IPAddress + ' -pw ' + Password + ' -b ' + '"' + sBatchFile + '"';
    Process.Parameters.Add(S);
    //Process.Options := Process.Options + [poWaitOnExit, poUsePipes];
    Process.Options := Process.Options + [poWaitOnExit];
    Process.Execute;
  finally
    Process.Free;
  end;

  // A temp Memorystream is used to buffer the output
  MemStream := TMemoryStream.Create;
  BytesRead := 0;

  OurProcess := TProcess.Create(nil);
  // Recursive dir is a good example.
  OurCommand:='invalid command, please fix the IFDEFS.';
  {$IFDEF Windows}
  //Can't use dir directly, it's built in
  //so we just use the shell:
  OurCommand:='cmd.exe /c "dir /s c:\windows\"';
  {$ENDIF Windows}
  {$IFDEF Unix}
  //Needs to be tested on Linux/Unix:
  OurCommand := 'ls --recursive --all -l /';
  {$ENDIF Unix}
  writeln('-- Going to run: ' + OurCommand);
  OurProcess.CommandLine := OurCommand;

  // We cannot use poWaitOnExit here since we don't
  // know the size of the output. On Linux the size of the
  // output pipe is 2 kB; if the output data is more, we
  // need to read the data. This isn't possible since we are
  // waiting. So we get a deadlock here if we use poWaitOnExit.
  OurProcess.Options := [poUsePipes];
  WriteLn('-- External program run started');
  OurProcess.Execute;
  while OurProcess.Running do
  begin
    // make sure we have room
    MemStream.SetSize(BytesRead + READ_BYTES);

    // try reading it
    NumBytes := OurProcess.Output.Read((MemStream.Memory + BytesRead)^, READ_BYTES);
    if NumBytes > 0
    then begin
      Inc(BytesRead, NumBytes);
      Write('.') //Output progress to screen.
    end
    else begin
      // no data, wait 100 ms
      Sleep(100);
    end;
  end;
  // read last part
  repeat
    // make sure we have room
    MemStream.SetSize(BytesRead + READ_BYTES);
    // try reading it
    NumBytes := OurProcess.Output.Read((MemStream.Memory + BytesRead)^, READ_BYTES);
    if NumBytes > 0
    then begin
      Inc(BytesRead, NumBytes);
      Write('.');
    end;
  until NumBytes <= 0;
  if BytesRead > 0 then WriteLn;
  MemStream.SetSize(BytesRead);
  WriteLn('-- External program run complete');

  OutputLines := TStringList.Create;
  OutputLines.LoadFromStream(MemStream);
  WriteLn('-- External program output line count = ', OutputLines.Count, ' --');
  for NumBytes := 0 to OutputLines.Count - 1 do
  begin
    WriteLn(OutputLines[NumBytes]);
  end;
  WriteLn('-- Program end');
  OutputLines.Free;
  OurProcess.Free;
  MemStream.Free;
end;

procedure TDBInterface.IssueCommandToServer(const IPAddress,Password,
  Command: string);
var
  //SL,SLOutput: TStringList;
  S,sPSFTPFile: string;
  Process: TProcess;
begin
  // e.g. plink root@192.168.0.6 -pw password fs_cli -x reloadxml

  Process := TProcess.Create(nil);
  try
    sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'plink.exe';

    if not FileExists(sPSFTPFile) then
      raise Exception.Create('Unable to find the file "' + sPSFTPFile + '"');

    Process.Executable := sPSFTPFile;
    S := '-pw'; // + Password + ' ' + Username + '@' + IPAddress + ' ' + Command;

    Process.Parameters.Add(S);
    S := Password;
    Process.Parameters.Add(S);
    S := cRootUser + '@' + IPAddress;
    Process.Parameters.Add(S);
    S := Command;
    Process.Parameters.Add(S);
    Process.Options := Process.Options + [poWaitOnExit];
    Process.Execute;
  finally
    Process.Free;
  end;
end;

procedure TDBInterface.IssueCommandToServerNoWait(const IPAddress,Password,
  Command: string);
var
  //SL,SLOutput: TStringList;
  S,sPSFTPFile: string;
  Process: TProcess;
begin
  // e.g. plink root@192.168.0.6 -pw password fs_cli -x reloadxml

  Process := TProcess.Create(nil);
  try
    sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'plink.exe';

    if not FileExists(sPSFTPFile) then
      raise Exception.Create('Unable to find the file "' + sPSFTPFile + '"');

    Process.Executable := sPSFTPFile;
    S := '-pw'; // + Password + ' ' + Username + '@' + IPAddress + ' ' + Command;

    Process.Parameters.Add(S);
    S := Password;
    Process.Parameters.Add(S);
    S := cRootUser + '@' + IPAddress;
    Process.Parameters.Add(S);
    S := Command;
    Process.Parameters.Add(S);
    //Process.Options := Process.Options + [poWaitOnExit];
    Process.Execute;
  finally
    Process.Free;
  end;
end;

procedure TDBInterface.ReloadXML(const IPAddress,Password: string);
var
  SLOutput: TStringList;
  S,sPSFTPFile: string;
  Process: TProcess;
begin
  // plink root@192.168.0.6 -pw password fs_cli -x reloadxml

  Process := TProcess.Create(nil);
  try
    sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'plink.exe';
    Process.Executable := sPSFTPFile;
    S := cRootUser + '@' + IPAddress + ' -pw ' + Password + ' fs_cli -x reloadxml';
    Process.Parameters.Add(S);
    Process.Options := Process.Options + [poWaitOnExit, poUsePipes];
    //Process.Options := Process.Options + [poWaitOnExit];
    Process.Execute;

    SLOutput := TStringList.Create;
    try
      SLOutput.LoadFromStream(Process.Output);
      // Save the output to a file.
      SLOutput.SaveToFile(IncludeTrailingBackslash(fRockFSCB.WorkingDir) + 'output.txt');
    finally
      SLOutput.Free;
    end;
  finally
    Process.Free;
  end;
end;

end.

