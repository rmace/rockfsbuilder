unit rockfstypes;

{$mode objfpc}{$H+}

interface

uses
  ExtCtrls, SysUtils;

const
  // File constants
  cPSFTPBatchFile      = 'batchfile.scr';
  cpsftpFile           = 'psftp.exe';
  cpsLinkFile          = 'pslink.exe';
  cFSInstallScriptFile = 'fs-debian-installer-pkgs.sh';

  // URL's
  cpsftpDownloadURL    = 'http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html';

  cAppName           = 'RoCKSwitch Builder';
  cFreeSWITCHName    = 'FreeSWITCH';
  cFreeSWITCHVersion = '1.6';
  cAppVersion        = '1.0 BETA 2';
  cRootUser          = 'root';

  // Debian distro used. Critical for identification in sources.list etc
  cDebianName    = 'Debian';
  cDebianDistro  = 'jessie';
  cDebianVersion = '8';

  // Dialplan\Default.xml constants

  // Gateway contstants

type
  TFNAActionType = (fnaatAddOrReplace,
                    fnaatDelete);

implementation

end.
