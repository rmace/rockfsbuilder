unit rockfscb;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, INIFiles;

type
  TRockFSCB = class
  private
    fFirstTimeRun: boolean;

    // Form dimensions
    fFormTop: integer;
    fFormLeft: integer;
    fFormWidth: integer;
    fFormHeight: integer;
    fSplitterPos: integer;

    fLastIPAddress: string;
    fLastViewedTab: integer;
    fRegistrationCode: integer;

    fFreeSWITCHConfDir: string;
    fWorkingDir: string;
  public
    constructor Create;
    destructor Destroy; override;

    property FirstTimeRun: boolean read fFirstTimeRun write fFirstTimeRun;

    // Form dimensions
    property FormTop: integer read fFormTop write fFormTop;
    property FormLeft: integer read fFormLeft write fFormLeft;
    property FormWidth: integer read fFormWidth write fFormWidth;
    property FormHeight: integer read fFormHeight write fFormHeight;

    property SplitterPos: integer read fSplitterPos write fSplitterPos;

    property LastIPAddress: string read fLastIPAddress write fLastIPAddress;
    property LastViewedTab: integer read fLastViewedTab write fLastViewedTab;
    property RegistrationCode: integer read fRegistrationCode write fRegistrationCode;

    property FreeSWITCHConfDir: string read fFreeSWITCHConfDir write fFreeSWITCHConfDir;
    property WorkingDir: string read fWorkingDir write fWorkingDir;

    procedure Read;
    procedure Write;
  end;

implementation

constructor TRockFSCB.Create;
begin
  inherited Create;

  fFormHeight := 430;
  fFormLeft := 50;
  fFormTop := 50;
  fFormWidth := 680;
  fSplitterPos := 300;

  fFreeSWITCHConfDir := '';
  fLastIPAddress := '';
  fRegistrationCode := 0;
  fWorkingDir := GetAppConfigDir(False);

  fFirstTimeRun := True;
end;

destructor TRockFSCB.Destroy;
begin
  inherited Destroy;
end;

procedure TRockFSCB.Read;
var
  INIStore: TINIFile;
begin
  if not DirectoryExists(GetAppConfigDir(False)) then
    if not CreateDir(GetAppConfigDir(False)) then
      raise Exception.Create('Unable to create directory ' + GetAppConfigDir(False) + ' ' + SysErrorMessage(GetLastOSError));
  INIStore := TINIFile.Create(GetAppConfigFile(False));
  try
    with INIStore do
      begin
        fFirstTimeRun  := ReadBool   ('General','FirstTimeRun'     ,fFirstTimeRun);

        fLastViewedTab := ReadInteger('General','LastViewedTab'    ,fLastViewedTab);

        // Main form position
        fFormTop       := ReadInteger('General','FormTop'    ,fFormTop);
        fFormLeft      := ReadInteger('General','FormLeft'   ,fFormLeft);
        fFormWidth     := ReadInteger('General','FormWidth'  ,fFormWidth);
        fFormHeight    := ReadInteger('General','FormHeight' ,fFormHeight);

        fRegistrationCode  := ReadInteger('General','RegistrationCode' ,fRegistrationCode);
        fLastIPAddress     := ReadString ('General','LastIPAddress'    ,fLastIPAddress);
        ffreeSWITCHConfDir := ReadString ('General','freeSWITCHConfDir',ffreeSWITCHConfDir);
        fWorkingDir        := ReadString ('General','WorkingDir'       ,fWorkingDir);
      end;
  finally
    INIStore.Free;
  end;
end;

//******************************************************************************
// Write
//******************************************************************************

procedure TRockFSCB.Write;
var
  INIStore: TINIFile;
begin
  if not DirectoryExists(GetAppConfigDir(False)) then
    if not CreateDir(GetAppConfigDir(False)) then
      raise Exception.Create('Unable to create directory ' + GetAppConfigDir(False) + ' ' + SysErrorMessage(GetLastOSError));

  INIStore := TINIFile.Create(GetAppConfigFile(False));
  try
    with INIStore do
      begin
        WriteBool   ('General','FirstTimeRun'      ,fFirstTimeRun);

        WriteInteger('General','LastViewedTab'     ,fLastViewedTab);

        // Main form position

        WriteInteger('General','FormTop'    ,fFormTop);
        WriteInteger('General','FormLeft'   ,fFormLeft);
        WriteInteger('General','FormWidth'  ,fFormWidth);
        WriteInteger('General','FormHeight' ,fFormHeight);
        WriteInteger('General','SplitterPos',fSplitterPos);

        WriteInteger('General','RegistrationCode'  ,fRegistrationCode);
        WriteString ('General','freeSWITCHConfDir' ,ffreeSWITCHConfDir);
        WriteString ('General','LastIPAddress'     ,fLastIPAddress);
        WriteString ('General','WorkingDir'        ,fWorkingDir);
      end;
  finally
    INIStore.Free;
  end;
end;

end.


