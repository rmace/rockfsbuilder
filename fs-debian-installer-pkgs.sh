#!/bin/bash
apt-get update && apt-get -y install curl
curl http://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub | apt-key add -
echo 'deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main' >> /etc/apt/sources.list.d/freeswitch.list
apt-get update
apt-get -y install freeswitch-all freeswitch-all-dbg gdb
cp -a /usr/share/freeswitch/conf/vanilla /etc/freeswitch
