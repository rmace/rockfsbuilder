program RoCKSwitchBuilder;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, main, connect
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='RoCKSwitch Builder';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TfrmMain,frmMain);
  Application.Run;
end.

