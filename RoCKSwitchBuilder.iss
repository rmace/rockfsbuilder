; -- Example2.iss --
; Same as Example1.iss, but creates its icon in the Programs folder of the
; Start Menu instead of in a subfolder, and also creates a desktop icon.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=RoCKSwitch Builder
AppVersion=1.0 BETA 1
CloseApplications=yes
DefaultDirName={pf}\RoCKSwitch Builder
; Since no icons will be created in "{group}", we don't need the wizard
; to ask for a Start Menu folder name:
DisableProgramGroupPage=yes
UninstallDisplayIcon={app}\RoCKSwitchBuilder.exe
AppPublisher=RoCK Software
AppPublisherURL=http://www.rocksoftware.co.uk/
;OutputDir=userdocs:Inno Setup Examples Output
;SetupLogging=yes
OutputBaseFilename=RoCKSwitchBuilderSetup
OutputDir=C:\projects\rockfsbuilder\
SignTool=kSign /d $qRoCKSwitch Builder$q /du $qhttp://www.rocksoftware.co.uk$q $f
;LicenseFile=eula.txt

[Files]
Source: "RoCKSwitchBuilder.exe"; DestDir: "{app}"
Source: "plink.exe"; DestDir: "{app}"
Source: "psftp.exe"; DestDir: "{app}"
Source: "README-FIRST.txt"; DestDir: "{app}"

Source: "Follow us on Facebook.url"; DestDir: "{app}";
Source: "Follow us on Twitter.url"; DestDir: "{app}";
Source: "RoCKSwitch Builder Website.url"; DestDir: "{app}";

Source: "C:\projects\rockfsbuilder\images\facebook.ico"; DestDir: "{app}";
Source: "C:\projects\rockfsbuilder\images\twitter_logo.ico"; DestDir: "{app}";
Source: "C:\projects\rockfsbuilder\images\www_page.ico"; DestDir: "{app}";


[Icons]
;Name: "{commonprograms}\RoCK-Do\RDRunner"; Filename: "{app}\RDRunner.exe"
;Name: "{commonprograms}\Quick Start Guide"; Filename: "{app}\RoCKNet Quick Set Up Guide.pdf"
;Name: "{commondesktop}\RoCKN[Icons]
Name: "{commonprograms}\RoCKSwitch Builder\RoCKSwitch Builder"; Filename: "{app}\RoCKSwitchBuilder.exe";
Name: "{commonprograms}\RoCKSwitch Builder\ReadMe First"; Filename: "{app}\README-FIRST.txt";
Name: "{commonprograms}\RoCKSwitch Builder\Follow us on Facebook"; Filename: "{app}\Follow us on Facebook.url"; IconFilename: "{app}\facebook.ico";
Name: "{commonprograms}\RoCKSwitch Builder\Follow us on Twitter"; Filename: "{app}\Follow us on Twitter.url"; IconFilename: "{app}\twitter_logo.ico";
Name: "{commonprograms}\RoCKSwitch Builder\RoCKSwitch Builder website"; Filename: "{app}\RoCKSwitch Builder Website.url"; IconFilename: "{app}\www_page.ico";