unit connect;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TfrmConnect }

  TfrmConnect = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    edtIPAddress: TEdit;
    edtRootPassword: TEdit;
    lblIPAddress: TLabel;
    llbRootPassword: TLabel;
    procedure btnOKClick(Sender: TObject);
  private
    { private declarations }
  public
    function ShowModal(var RootPassword, IPaddress: string): integer; reintroduce; overload;
  end;

var
  frmConnect: TfrmConnect;

implementation

{$R *.lfm}

{ TfrmConnect }

procedure TfrmConnect.btnOKClick(Sender: TObject);
begin
  if Trim(edtIPAddress.Text)  = '' then
    begin
      ShowMessage('Please enter an IP address');
      edtIPAddress.SetFocus;
      Exit;
    end;

  if Trim(edtRootPassword.Text)  = '' then
    begin
      ShowMessage('Please enter the root password');
      edtRootPassword.SetFocus;
      Exit;
    end;

  ModalResult := mrOK;
end;

function TfrmConnect.ShowModal(var RootPassword, IPaddress: string): integer;
begin
  edtIPAddress.Text := IPaddress;

  Result := inherited ShowModal;

  if ModalResult = mrOK then
    begin
      RootPassword := Trim(edtRootPassword.Text);
      IPaddress := Trim(edtIPAddress.Text);
    end;
end;

end.

