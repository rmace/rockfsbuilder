To enable SSH login for a root user on Debian Linux system you need to first configure SSH server.

Open /etc/ssh/sshd_config and change the following line:
FROM:
PermitRootLogin without-password
TO:
PermitRootLogin yes

Once you made the above change restart your SSH server:
# /etc/init.d/ssh restart
[ ok ] Restarting ssh (via systemctl): ssh.service.
From now on you will be able to ssh login as a root: