unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  StdCtrls,ECLink, DBInterface,rockfstypes,rfsprogress,rockfscb;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    btnNew: TBitBtn;
    btnUpdate: TBitBtn;
    btnChangeIP: TBitBtn;
    btnClose: TButton;
    chkDebug: TCheckBox;
    lnkWebsite: TECLink;
    memNew: TMemo;
    memLog: TMemo;
    memUpdate: TMemo;
    memChangeIP: TMemo;
    procedure btnCloseClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    fFirstTimeRun: boolean;
    ffrmProgress: TfrmProgress;
    fCB: TRockFSCB;
    fRootPassword: string;
    fCurrentIP: string;
    fDBInterface: TDBInterface;
    procedure AddToLog(const Str: string);
    procedure DisplayMemoLinesCountIfNeeded(const Stage: integer);
    procedure GetConnectionDetails;
    procedure GetFileFromServer(const IPAddress, Password,SrcFolder, SrcFile, DestFolder: string; var Memo: TMemo);
    procedure IssueCommandToServer(const IPAddress, Password, Command: string; var Memo: TMemo);
    procedure PutFileOnServer(const IPAddress, Password, SrcFolder, SrcFile, DestFolder: string; var Memo: TMemo);
    procedure RemoveCDFromAPTSources(const IPAddress, Password: string; var Memo: TMemo);
    procedure UpdateProgressLabel(const S: string);
    procedure IncrementStatusBar;
  public
    { public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.lfm}

uses
  connect,AutoResponseProcess,StrUtils;

{ TfrmMain }

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  fFirstTimeRun := True;
end;

procedure RefreshMemo;
begin
  Application.ProcessMessages;
end;

procedure TfrmMain.AddToLog(const Str: string);
begin
  memLog.Lines.Add(Str);
end;

procedure TfrmMain.DisplayMemoLinesCountIfNeeded(const Stage: integer);
begin
  if chkDebug.Checked then
    ShowMessage('Memo line count for stage ' + IntToStr(Stage) + ' is: ' + IntToStr(ffrmProgress.memMain.Lines.Count));
end;

procedure TfrmMain.GetConnectionDetails;
var
  frmConnect: TfrmConnect;
begin
  frmConnect := TfrmConnect.Create(Self);
  try
    fCB.Read;
    fCurrentIP := fCB.LastIPAddress;
    if frmConnect.ShowModal(fRootPassword,fCurrentIP) =mrOK then
      begin
        fCB.Read;
        fCB.LastIPAddress := fCurrentIP;
        fCB.Write;
      end;
  finally
    frmConnect.Release;
  end;
end;

procedure TfrmMain.GetFileFromServer(const IPAddress,Password,SrcFolder,SrcFile,
  DestFolder: string; var Memo: TMemo);
var
  //I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile: string;
begin
  // This code will get a file from the server.

  // First, let's create the batchfile
  SL := TStringList.Create;
  try
    S := 'cd ' + '"' + SrcFolder + '"';
    SL.Add(S);
    S := 'lcd ' + '"' + DestFolder + '"';
    SL.Add(S);
    S := 'get ' + '"' + SrcFile + '"'; // "' + sDestFolder + '"';
    SL.Add(S);
    SL.Add('quit');
    sBatchFile := IncludeTrailingBackslash(fCB.WorkingDir) + cPSFTPBatchFile;
    SL.SaveToFile(sBatchFile);
  finally
    SL.Free;
  end;

  sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'psftp.exe';
  S := cRootUser + '@' + IPAddress + ' -pw ' + Password + ' -b ' + '"' + sBatchFile + '"';
  ExecProcess(sPSFTPFile,[S],Memo.Lines,['Update cached key?','Store key in cache? (y/n)'],['y','y'],@RefreshMemo);
end;

procedure TfrmMain.IssueCommandToServer(const IPAddress, Password,
  Command: string; var Memo: TMemo);
var
  //SL,SLOutput: TStringList;
  S,sPassword,sUsername,sCommand,sPSFTPFile: string;
begin
  // e.g. plink root@192.168.0.6 -pw password fs_cli -x reloadxml

  sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'plink.exe';
  S := '-pw'; // + Password + ' ' + Username + '@' + IPAddress + ' ' + Command;

  sPassword := Password;
  sUsername := cRootUser + '@' + IPAddress;
  sCommand := Command;
  ExecProcess(sPSFTPFile,[S,sPassword,sUsername,sCommand],Memo.Lines,['Update cached key?','Store key in cache? (y/n)'],['y','y'],@RefreshMemo);
end;

procedure TfrmMain.PutFileOnServer(const IPAddress, Password, SrcFolder,
  SrcFile, DestFolder: string; var Memo: TMemo);
var
  //I: integer;
  SL: TStringList;
  S,sBatchFile,sPSFTPFile: string;
begin
  // This code will put a file on the server.

  // First, let's create the batchfile
  SL := TStringList.Create;
  try
    S := 'lcd ' + '"' + SrcFolder + '"';
    SL.Add(S);
    S := 'cd ' + '"' + DestFolder + '"';
    SL.Add(S);
    S := 'put ' + '"' + SrcFile + '"'; // "' + sDestFolder + '"';
    SL.Add(S);
    SL.Add('quit');
    sBatchFile := IncludeTrailingBackslash(fCB.WorkingDir) + cPSFTPBatchFile;
    SL.SaveToFile(sBatchFile);
  finally
    SL.Free;
  end;

  sPSFTPFile := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'psftp.exe';
  S := cRootUser + '@' + IPAddress + ' -pw ' + Password + ' -b ' + '"' + sBatchFile + '"';
  UpdateProgressLabel('Uploading "' + SrcFile + '" to server...');
  ExecProcess(sPSFTPFile,[S],Memo.Lines,['Store key in cache? (y/n)'],['y'],@RefreshMemo);
  IncrementStatusBar;
end;

procedure TfrmMain.RemoveCDFromAPTSources(const IPAddress, Password: string;
  var Memo: TMemo);
var
  bEntryFound: boolean;
  I: integer;
  SL: TStringList;
  S,{sBatchFile,sPSFTPFile,}sSrcFolder,sSrcFile,sDestFolder: string;
  //Process: TProcess;
begin
  bEntryFound := False;

  // This code will retrieve the /etc/apt/source.list file from the server.
  // Then, it will comment out the "deb cdrom" line (if needed)
  // Then, upload it again to the server

  sSrcFolder := '/etc/apt/';
  sSrcFile := 'sources.list';
  sDestFolder := IncludeTrailingBackslash(fCB.WorkingDir);

  GetFileFromServer(IPAddress,Password,sSrcFolder,sSrcFile,sDestFolder,ffrmProgress.memMain);

  if not FileExists(sDestFolder + sSrcFile) then // Check to make sure we got the file
    raise Exception.Create('Unable to find the sources.list file');

  // OK, we now have the sources file, so comment out the CD 1 if need be.
  SL := TStringList.Create;
  try
    SL.LoadFromFile(sDestFolder + sSrcFile);
    for I := 0 to SL.Count -1 do
      begin
        S := SL[I];
        if AnsiContainsStr(S,'deb cdrom:') then
          if not AnsiStartsStr('#',S) then
            begin
              // We've found an entry that hasn't been commented out
              bEntryFound := True;
              S := '#' + S;
              SL[I] := S;
              SL.SaveToFile(sDestFolder + sSrcFile);
            end;
      end;
  finally
    SL.Free;
  end;
  if bEntryFound then
    PutFileOnServer(IPAddress,Password,sDestFolder,sSrcFile,sSrcFolder,ffrmProgress.memMain);
end;

procedure TfrmMain.UpdateProgressLabel(const S: string);
begin
  memLog.Lines.Add(S);

  ffrmProgress.lblInfo.Caption := S;
  Application.ProcessMessages;
end;

procedure TfrmMain.IncrementStatusBar;
begin
  ffrmProgress.pbMain.Position := ffrmProgress.pbMain.Position +1;
end;

procedure TfrmMain.FormActivate(Sender: TObject);
begin
  if fFirstTimeRun then
    begin
      fFirstTimeRun := False;
      fCB := TRockFSCB.Create;
      fDBInterface := TDBInterface.Create;
      ffrmProgress := TfrmProgress.Create(Self);

      frmMain.Caption := cAppName + ' v' + cAppVersion;

      memNew.Text      := 'This will install a fresh installation of ' + cFreeSWITCHName + ' ' + cFreeSWITCHVersion + #13#13 +
                          'Use this option, if you have just finished a fresh installation of ' + cDebianName + ' ' + cDebianVersion + ' (' + cDebianDistro + ') AMD64';
      memUpdate.Text   := 'This will update any existing installation of ' + cFreeSWITCHName + ' ' + cFreeSWITCHVersion  + #13#13 +
                          'Use this option to perform an update of the operating system software (Debian 8) and FreeSWITCH None of your existing configuration will be changed.';
      memChangeIP.Text := 'Use this option to change the IP address of the FreeSWITCH box. (requires reboot)';
    end;
end;

procedure TfrmMain.btnNewClick(Sender: TObject);
begin
  // Get connection details if requried.
  if (fRootPassword = '') or (fCurrentIP = '') then
    GetConnectionDetails;

  if (fRootPassword = '') or (fCurrentIP = '') then
    begin
      ShowMessage('Incorrect connection details were entered');
      Exit;
    end;

  ffrmProgress.ShowOnTop;

  ffrmProgress.pbMain.Position := 0;
  ffrmProgress.pbMain.Max      := 5;
  UpdateProgressLabel('Preparing package index (Step 1/8)...');
  RemoveCDFromAPTSources(fCurrentIP,fRootPassword,ffrmProgress.memMain);
  DisplayMemoLinesCountIfNeeded(1);
  Application.ProcessMessages;

  UpdateProgressLabel('Updating package index (Step 2/8)...');
  ffrmProgress.pbMain.Max := 16;
  IssueCommandToServer(fCurrentIP,fRootPassword,'apt-get update',ffrmProgress.memMain);
  DisplayMemoLinesCountIfNeeded(2);

  UpdateProgressLabel('Installing curl (Step 3/8)...');
  ffrmProgress.pbMain.Max := 52;
  IssueCommandToServer(fCurrentIP,fRootPassword,'apt-get install -y curl',ffrmProgress.memMain);
  DisplayMemoLinesCountIfNeeded(3);

  UpdateProgressLabel('Installing key (Step 4/8)...');
  ffrmProgress.pbMain.Max := 12;
  IssueCommandToServer(fCurrentIP,fRootPassword,'curl https://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub | apt-key add -',ffrmProgress.memMain);
  DisplayMemoLinesCountIfNeeded(4);

  UpdateProgressLabel('Adding FreeSWITCH sources to apt (Step 5/8)...');
  ffrmProgress.pbMain.Max := 1;
  IssueCommandToServer(fCurrentIP,fRootPassword,'echo "deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main" > /etc/apt/sources.list.d/freeswitch.list',ffrmProgress.memMain);
  DisplayMemoLinesCountIfNeeded(5);

  UpdateProgressLabel('Updating package index (Step 6/8)...');
  ffrmProgress.pbMain.Max := 20;
  IssueCommandToServer(fCurrentIP,fRootPassword,'apt-get update',ffrmProgress.memMain);
  DisplayMemoLinesCountIfNeeded(6);

  UpdateProgressLabel('Installing FreeSWITCH 1.6 (Step 7/8)...');
  ffrmProgress.pbMain.Max := 1121;
  IssueCommandToServer(fCurrentIP,fRootPassword,'apt-get install -y freeswitch-all freeswitch-all-dbg gdb',ffrmProgress.memMain);
  DisplayMemoLinesCountIfNeeded(7);

  UpdateProgressLabel('Rebooting (Step 8/8)...');
  ffrmProgress.pbMain.Max := 1;
  IssueCommandToServer(fCurrentIP,fRootPassword,'shutdown -r -t +1',ffrmProgress.memMain);
  ffrmProgress.Close;
  AddToLog('FreeSWITCH has now been installed on your server, and will be avilable, after a re-boot, which will happen in less than 1 minute');
  DisplayMemoLinesCountIfNeeded(8);

  //UpdateProgressLabel('Downloading software (Step 3/5)...');
  //IssueCommandToServer('root',fLiveIPAddress,fRootPassword,'wget http://www.rocksolidvoip.co.uk/downloads/fs-debian-installer-pkgs.sh',Memo);
  //ffrmProgress.pbMain.Max := 449;
  //ffrmProgress.lblInfo.Caption := 'Performing install, please be patient as the last few steps can take a while (Step 4/5)...';
  //IssueCommandToServer('root',fLiveIPAddress,fRootPassword,'chmod +x ./fs-debian-installer-pkgs.sh && ./fs-debian-installer-pkgs.sh',Memo);
  //fRemoteWorkComplete := True;
  //memStatus.Lines.Add('Installation complete, rebooting server...');
  //IssueCommandToServer('root',fLiveIPAddress,fRootPassword,'reboot',ffrmProgress.memMain);
  //ffrmProgress.Close;







  {if frmConnect.ShowModal(fRootPassword,fCurrentIP) = mrOK then
    begin
      AddToLog('Creating new installation of FreeSWITCH...');
      AddToLog('Checking if CD line needs commenting out in /etc/apt/sources.lst');
      fDBInterface.RemoveCDFromAPTSources(fCurrentIP,fRootPassword);
      if fDBInterface.freeSWITCHSourcesInAPTSources(fCurrentIP,fRootPassword) then
        begin
          AddToLog('Found existing FreeSWITCH repo in /etc/apt/sources.lst');
        end
      else
        begin
          AddToLog('Adding FreeSWITCH repo to /etc/apt/sources.lst.d/ and installing...');
          fdbInterface.AddFreeSWITCHSourcesToAPTSourcesAndInstall(fCurrentIP,fRootPassword);
        end;
      AddToLog('Done.');
      AddToLog('Rebooting server.');
      //AddToLog('Updating new /etc/apt/sources.lst...');
      fDBInterface.IssueCommandToServerNoWait(fCurrentIP,fRootPassword,'reboot');
      ShowMessage('Install complete.');
    end;}

  //memStatus.Lines.Add('Performing install...');
  //fDBInterface.IssueCommandToServer(sUser,sIPAddress,sPassword,'wget http://www.rocksolidvoip.co.uk/downloads/fs-debian-installer-pkgs.sh');
  //fDBInterface.IssueCommandToServer(sUser,sIPAddress,sPassword,'chmod +x ./fs-debian-installer-pkgs.sh && ./fs-debian-installer-pkgs.sh');
  //fRemoteWorkComplete := True;
  //memStatus.Lines.Add('Installation complete, rebooting server...');
  //fDBInterface.IssueCommandToServer(sUser,sIPAddress,sPassword,'reboot');
  //rbRemoteAlreadyInstalled.Checked := True;

end;

procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  Close;
end;

end.

